import { Alert } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function NotFoundPage() {
    return (
        <div className="container pt-3">
            <Alert key={0} variant='danger'>
                Error: 404 <br />
                <Link className="text-dark" to={'/'}>Volver página principal</Link>
            </Alert>
        </div>
    )
}
