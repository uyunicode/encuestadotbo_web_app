import { Component } from "react";
import { Button } from "react-bootstrap";
import CarouselPictures from "../components/CarouselPictures";

export default class HomePage extends Component {
    render() {
        return (
            <div className="p-3">
                <CarouselPictures />
                <Button variant="primary" style={
                    { float: 'right', marginRight: '25px', overflow: 'auto', borderRadius: '15px', boxShadow: '5px 5px rgba(0, 0, 0, 0.1)' }}>
                    Crear Encuesta
                </Button>
            </div>
        );
    }
}
