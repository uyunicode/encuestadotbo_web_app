import { Component } from 'react';

import Carousel from 'react-bootstrap/Carousel';
import { Col, Image } from 'react-bootstrap';
import FormsRafiki from '../components/assets/forms-rafiki.svg';
import ChecklistBro from '../components/assets/checklist-bro.svg';
import TeamWorkAmico from '../components/assets/team-work-amico.svg';

export default class CarouselPictures extends Component {
    render() {
        return (
            <Carousel>
                <Carousel.Item>
                    <Col xs={6} md={4}>
                        <Image src={FormsRafiki} />
                    </Col>
                    <Carousel.Caption>
                        <h3>Gestón de Encuestas</h3>
                        <p>Software para la creación de encuestas online de <br /> satisfacción del cliente, investigación de mercados, estudios de opinión, etc.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Col xs={6} md={4}>
                        <Image src={ChecklistBro} />
                    </Col>
                    <Carousel.Caption>
                        <h3>Crea encuestas fácilmente. <br />Recibe respuestas rápido.</h3>
                        <p>Es gratis!!! Ahorra tiempo y dinero Sin contratos, sin obligaciones Sólo crea encuestas Gratis</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Col xs={6} md={4}>
                        <Image src={TeamWorkAmico} />
                    </Col>
                    <Carousel.Caption>
                        <h3>Trabajo en equipo</h3>
                        <p>Contamos con un equipo agil en el plantel <br /> administrativos, y gente recolectora de datos por la ciudad de Santa Cruz</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        )
    }
}
