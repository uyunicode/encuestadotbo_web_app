import { Component } from 'react'
import { Col, Row } from 'react-bootstrap'

export default class Footer extends Component {
    render() {
        return (
            <footer style={{
                position: 'fixed',
                left: '0',
                bottom: '0',
                width: '100%'
            }} className="text-center bg-dark text-white">
                <div className="text-center p-3">
                    <Row>
                        <Col>
                            😁😁 2022 Copyright: -
                            <a className="text-white" href="https://uyunicode.vercel.app/index.html"> uyunicode.com</a>
                        </Col>
                        <Col></Col>
                        <Col>
                            <a className="text-white" href="https://storyset.com/online">Partner: Online illustrations by Storyset</a>
                        </Col>
                    </Row>
                </div>
                {/* Copyright */}
            </footer>
        )
    }
}
