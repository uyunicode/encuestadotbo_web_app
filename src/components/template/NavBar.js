import { Component } from 'react';
import { Navbar, Container } from 'react-bootstrap';

export default class NavBar extends Component {
    render() {
        return (
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="/">
                        <img
                            src="https://static-s.aa-cdn.net/img/gp/20600008453377/aSjWtiNRMNQ-Msd6IazRh4vsP3BFjJJHpPcESrx-ukpO5KAXFyvsEfs3cYQBAOIj6Co=w300?v=1"
                            alt="logo-uagrm"
                            width="40" height="40" />
                        {' '}encuesta.bo</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    {/* <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">

                        </Nav>
                        <Nav>
                            <Nav.Link eventKey={2} to="#">
                                Ayuda
                            </Nav.Link>
                            <Nav.Link eventKey={2} to="/login">
                                Login
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse> */}
                </Container>
            </Navbar>
        );
    }
}
